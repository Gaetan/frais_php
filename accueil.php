<?php
session_start(); // ouverture d'une session ($_SESSION)
require 'inc/db.php'; // fichier de co a la BDD
unset($_POST['submitD']);

$req = $PDO->prepare('SELECT * FROM travel WHERE user_id = '.$_SESSION['id']);
$req->execute();
$datasT = $req->fetchALL();

$req = $PDO->prepare('SELECT * FROM travel WHERE user_id = '.$_SESSION['id'].' ORDER BY amont');
$req->execute();
$test = $req->fetchALL();


 ?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Pop Expens</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/default.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Lobster|Poiret+One" rel="stylesheet">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <link rel="stylesheet" href="css/01.css">
</head>
<body>
  <div class="container-fluid">
    <header>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a id="popexpens" class="navbar-brand" href="#">Pop Expens</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="accueil.php">Accueil <span class="sr-only">(current)</span></a></li>
              <li><a href="deplacement.php">Ajouter un déplacement</a></li>
              <li><a href="setting.html">Paramètres</a></li>
              <li><a href="deconnexion.php">Déconnexion</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>
    <main>
      <?php if (empty($datasT)): ?>
          <div class="row"></div>
          <div class="col-xs-6 col-xs-offset-3 col-md-4">
            <p id="textmain">Vous n'avez pas encore effectué de déplacement</p>
          </div>
          <div class="row"></div>
          <div class="img col-xs-2 col-xs-offset-8 col-md-4">
            <a  id="imgA" href="deplacement.php"><img src="img/add.png" alt="add" width="80"></a>
          </div>
        <?php endif; ?>
        <?php if (!empty($datasT)): ?>
          <table>
            <thead>
              <tr>
                <th>Sujet</th>
                <th>Date</th>
                <th>Lieux départ</th>
                <th>Lieux arriver</th>
                <th>Nombre de Kilométre</th>
                <th>Ramboursement</th>
                <th>Prenom</th>
                <th>Nom</th>
              </tr>
            </thead>
            <tbody>
          <?php foreach ($datasT as $k => $v): ?>
            <tr>
               <td><?php echo $v->subject; ?></td>
               <td><?php echo $v->dateT; ?></td>
               <td><?php echo $v->start; ?></td>
               <td><?php echo $v->arrival; ?></td>
               <td><?php echo $v->km; ?></td>
               <td><?php echo $v->amont; ?></td>
               <td><?php echo $_SESSION['firstname']; ?></td>
               <td><?php echo $_SESSION['lastname']; ?></td>
             </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
      </table>
    </div>
  </main>
</div>
</body>
</html>
