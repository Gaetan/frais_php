<?php
session_start(); // ouverture d'une session ($_SESSION)
require 'inc/db.php'; // fichier de co a la BDD

$req = $PDO->prepare('SELECT * FROM user');
$req->execute();
$datas = $req->fetchALL();
// récupération des utilisateur inscrit

if (!empty($_POST)) {
  foreach ($datas as $k => $v) {
    if ($_POST['email'] == $v->email && md5($_POST['password']) == $v->password) { // vérification de la combinaison mdp/mail
      // md5 pour le criptage
      $_SESSION['firstname'] = $v->firstname;
      $_SESSION['lastname'] = $v->lastname;
      $_SESSION['email'] = $v->email;
      $_SESSION['id'] = $v->id;
      // stocage des information dans la variable $_SESSION
      header('Location:accueil.php');
      exit();
      // redirection vers la page accueil.php
    }
  }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Pop Expens</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/default.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Lobster|Poiret+One" rel="stylesheet">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <link rel="stylesheet" href="css/01.css">
</head>
<body>
  <div class="container-fluid">
    <header>
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
          <h1 id="welcome">Bienvenue sur</h1>
          <h1 id="popexpens">Pop Expens</h1>
        </div>
      </div>
    </header>
    <main>
      <form id="formindex" action="index.php" method="post">
        <div class="form-group">
          <label id="labelindex" for="email">Adresse mail</label>
          <input type="email" class="form-control" name="email" placeholder="Adresse mail">
        </div>
        <div class="form-group">
          <label id="labelindex"  for="password">Mot de passe</label>
          <input type="password" class="form-control" name="password" placeholder="Mot de passe">
        </div>
        <div class="checkbox">
          <label id="labelindex">
            <input type="checkbox"> Se souvenir de moi
          </label>
        </div>
        <input id="labelindex" type="submit" name="submit" class="btn btn-default" value="Se connecter">
        <a id="labelindex" class="btn btn-default" href="inscription.php">S'enregistrer</a>
        <div id="forget" class="form-group">
          <a id="labelindex"  href="forgetpass.html">Mot de passe oublié ?</a>
        </div>
      </form>
      <?php

      require_once 'inc/db.php';

      if(!empty($_POST)&& !empty($_POST['email']) && !empty($_POST['password'])){


        $req = $pdo->prepare('SELECT * FROM user WHERE email = :email');
        $req->execute(['email' => $_POST['email']]);
        $user = $req->fetch();

        if($_POST['password'] === $user->password){
          session_start();
          header('location: index.php');
          exit();
        }else{
          $errors='Identifiant ou mot de passe incorrect';
        }
      }
      ?>
    </main>
  </div>
</body>
</html>
