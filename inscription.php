<?php
require 'inc/db.php'; // fichier de co a la bdd

if (isset($_POST['submit'])) {
  // requet
    $_POST['password'] = md5($_POST['password']);
     $req = $PDO->prepare('INSERT INTO user (lastname,firstname,email,password) VALUES(:lastname,:firstname,:email,:password)');
     $req->bindValue(':lastname', $_POST['lastname']);
     $req->bindValue(':firstname', $_POST['firstname']);
     $req->bindValue(':email', $_POST['email']);
     $req->bindValue(':password', $_POST['password']);
     $req->execute();
     // insetion dans la BDD
    header('Location:inscription2.html');
    exit();
    // redirection vers inscription2.php
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Pop Expens</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/default.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Lobster|Poiret+One" rel="stylesheet">

  <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
  <div class="container-fluid">
    <header>
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
          <h1 id="inscriptiontitle">Inscription</h1>
        </div>
      </div>
    </header>
    <main>
      <form id="formregister" action="inscription.php" method="post">
        <div class="form-group">
          <label id="labelindex" for="exampleInputText">Nom</label>
          <input type="text" class="form-control" id="exampleInputText" name="lastname" placeholder="Nom" required>
        </div>
        <div class="form-group">
          <label id="labelindex" for="exampleInputText">Prénom</label>
          <input type="text" class="form-control" id="exampleInputText" name="firstname" placeholder="Prénom" required>
        </div>
        <div class="form-group">
          <label id="labelindex" for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
          <label id="labelindex" for="exampleInputPassword1">Mot de passe</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Mot de passe" required>
        </div>
        <div class="form-group">
          <label id="labelindex" for="exampleInputPassword1">Confirmer mot de passe</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="password_repeat" placeholder="Confirmer mot de passe" required>
        </div>
        <input type="submit" name="submit" id="labelindex" class="btn btn-default">
      </form>
    </main>
  </div>
</body>
</html>
