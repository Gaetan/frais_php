$(function(){
  // Menu
  $('#on').on('click', function(){
    $('#menuD').css({
      'display': 'block'
    })
  })

  $('#off').on('click', function(){
    $('#menuD').css({
      'display': 'none'
    })
  })
// horsepower
  $('#type option').on('click', function(){
    if ($('#type').val() == 1) {
      $('#cvV').css({
        "display": "inline-block"
      })
      $('#cvM').css({
        "display": "none"
      })
      $('#km').attr("placeholder", "Kilométres (5000 max)")
    }else if ($('#type').val() == 2) {
      $('#cvM').css({
        "display": "inline-block"
      })
      $('#cvV').css({
        "display": "none"
      })
      $('#km').attr("placeholder", "Kilométres (3000 max)")
    }else if ($('#type').val() == 3) {
      $('#cvM').css({
        "display": "none"
      })
      $('#cvV').css({
        "display": "none"
      })
      $('#km').attr("placeholder", "Kilométres (2000 max)")
    }
  })
  // end horsepower
 // calc and error
	$('#calc').on('submit', function(){
		var km = $('#km').val()
		var cvV = $('#cvV').val()
		var cvM = $('#cvM').val()
    var type = $('#type').val()
    var check = $('input[name=voyage]:checked').val()
    var coeffV = {
      1 : 0.41,
      2 : 0.494,
      3 : 0.543,
      4 : 0.568,
      5 : 0.595,
    }
    var coeffM = {
      1 : 0.338,
      2 : 0.4,
      3 : 0.518
    }
    var coeffCM = {
      1 : 0.269,
    }

    console.log($('input[name=voyage]:checked').val());

    if (check == 3) {
      km = km * 2
    }else if (check === undefined) {
      $('#result').html("Merci de renseigner le type de déplacement")
      return false
    }

		if(isNaN(km) || km < 0 || km == "" || km > 5000){
			$('#km').css('background', 'red')
			$('#result').html("Problème rencontré au niveau des kilométres")
			return false
		}else{
			$('#km').css('background', 'white')
      if (type == 1) {
        total = (km * coeffV[cvV]).toFixed(2)
      }else if (type == 2) {
        if (km > 3000) {
      		$('#result').html("Problème rencontré au niveau des kilométres (pas plus de 3000)")
          return false
        }
        total = (km * coeffM[cvM]).toFixed(2)
      }else if (type == 3) {
        if (km > 2000) {
      		$('#result').html("Problème rencontré au niveau des kilométres (pas plus de 2000)")
          return false
        }
        total = (km * coeffCM[1]).toFixed(2)
      }
			$('#result').html("Votre indemnité kilométrique est de " + total + " €")
		}
	})

  $('#km').on('keyup', function(){
		var km = $('#km').val()
		if(isNaN(km) || km < 0 || km == "" || km > 5000){
			$('#km').css('background', 'red')
			$('#dTotal').html("")
			return false
		}else{
			$('#km').css('background', 'white')
		}
  })
})
