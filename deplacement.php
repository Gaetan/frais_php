<?php
session_start();
$errors = "";
$amont = "";

if (isset($_POST['submitD'])) {
  // calcule
  if ($_POST['nbCV'] == 1) {
    $amont = $_POST['km'] * 0.41;
  }elseif ($_POST['nbCV'] == 2) {
    $amont = $_POST['km'] * 0.494;
  }elseif ($_POST['nbCV'] == 3) {
    $amont = $_POST['km'] * 0.543;
  }elseif ($_POST['nbCV'] == 4) {
    $amont = $_POST['km'] * 0.568;
  }elseif ($_POST['nbCV'] >= 5) {
    $amont = $_POST['km'] * 0.595;
  }

  require 'inc/db.php';

  $req = $PDO->prepare('INSERT INTO travel (subject,dateT,start,arrival,km,amont,user_id) VALUES(:subject, :dateT, :start, :arrival, :km, :amont, :user_id)');
  $req->bindValue(':subject', $_POST['subject']);
  $req->bindValue(':dateT', date("Y-m-d"));
  $req->bindValue(':start', $_POST['start']);
  $req->bindValue(':arrival', $_POST['arrival']);
  $req->bindValue(':km', $_POST['km']);
  $req->bindValue(':amont', $amont);
  $req->bindValue(':user_id', $_SESSION['id']);
  $req->execute();
  // insetion dans la BDD
  header('Location:accueil.php');
  exit();
  // redirection vers inscription2.php
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Pop Expens</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/default.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Lobster|Poiret+One" rel="stylesheet">
  <!-- <script type="text/javascript" src="js/script.js"></script> -->
</head>
<body>
  <div class="container-fluid">
    <header>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a id="popexpens" class="navbar-brand" href="#">Pop Expens</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="accueil.php">Accueil</a></li>
              <li class="active"><a href="deplacement.php">Déplacement <span class="sr-only">(current)</span></a></li>
              <li><a href="setting.php">Paramètres</a></li>
              <li><a href="deconnexion.php">Déconnexion</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>
    <main>
      <h2>Déplacement</h2>
      <div id="formtravel">
        <div id="calc">
          <div class="row">
            <div id="buttons" class="col-xs-12">
              <div class="btn-group" data-toggle="buttons">
                <label for="1" class="btn btn-primary">
                  <input type="radio" name="voyage" id="aller" value="1" autocomplete="off">Aller
                </label>
                <label for="2" class="btn btn-primary">
                  <input type="radio" name="voyage" id="retour" value="2" autocomplete="off">Retour
                </label>
                <label for="3" class="btn btn-primary">
                  <input type="radio" name="voyage" id="aller-retour" value="3" autocomplete="off">Aller-Retour
                </label>
              </div>
            </div>
          </div>
          <form action="deplacement.php" method="post">
            <div class="form-group">
              <label id="labelindex" for="exampleInputEmail1">Objet</label>
              <input type="text" class="form-control" id="exampleInputEmail1" name="subject" placeholder="Objet">
            </div>
            <div class="form-group">
              <label id="labelindex" for="exampleInputPassword1">Lieu de départ</label>
              <input type="text" class="form-control" id="exampleInputPassword1" name="start" placeholder="Lieu de départ">
            </div>
            <div class="form-group">
              <label id="labelindex" for="exampleInputPassword1">Lieu d'arrivée</label>
              <input type="text" class="form-control" id="exampleInputPassword1" name="arrival" placeholder="Lieu d'arrivée">
            </div>
            <div class="form-group">
              <label id="labelindex" for="exampleInputPassword1">Nombre de kilométres</label>
              <input type="text" name="km" class="form-control" id="km" placeholder="Kilométres">
            </div>
            <div class="form-group">
              <label id="labelindex" for="exampleInputPassword1">Nombre de chevaux fiscau</label>
              <input type="number" name="nbCV" class="form-control" id="nbCV" placeholder="nombre de cv fiscaux">
            </div>
              <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                  <input type="submit" name="submitD">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="result">

      </div>
      <h2 id="dep">Frais</h2>
      <div id="formfees">
        <form>
          <div class="form-group">
            <label id="labelindex" for="exampleInputEmail1">Date de la dépense</label>
            <input type="Date" class="form-control" id="exampleInputEmail1" placeholder="">
          </div>
          <div class="form-group">
            <label id="labelindex" for="exampleInputPassword1">Montant</label>
            <input type="date" class="form-control" id="exampleInputPassword1" placeholder="">
          </div>
          <div class="form-group">
            <label id="labelindex" for="exampleInputPassword1">Type de dépense</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
          </div>
          <div class="form-group">
            <label id="labelindex" for="exampleInputFile">Justificatif</label>
            <input type="file" id="exampleInputFile">
          </div>
          <input type="submit" name="submitD" value="Confirmer">
        </form>
      </main>
      <footer>

      </footer>
    </div>
  </body>
  </html>
